﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    struct Vector
    {
        public int x, y;
        public Vector(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ")";
        }
        public static Vector operator +(Vector a, Vector b)
        {
            a.x += b.x;
            a.y += b.y;
            return a;
        }

        public static int operator *(Vector a, Vector b)
        {
            int c = a.x * b.x + a.y * b.y;
            return c;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Vector a = new Vector(1, 2);
            Vector b = new Vector(2, 3);
            Vector c = a + b;
            int d = a * b;
            Console.WriteLine(c);
            Console.WriteLine(d);
            Console.ReadKey();
        }
    }
}
